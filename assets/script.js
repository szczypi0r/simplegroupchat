var socket = io(),
currentUsers = [],
nickname = '',
colors = ['primary','success','info','warning','danger'];
 
$('form').submit(function(){
   socket.emit('message', $('#msg').val(), $("#nickName").val());
   $('#msg').val('');
   return false;
});
 
socket.on('message', function(msg, nickname){
   $('#chat').prepend('<div class="panel panel-' + colors[currentUsers.indexOf(nickname) % 5] + '"><div class="panel-heading"><h3 class="panel-title">' + nickname + '</h3></div>' +
   	'<div class="panel-body">' + msg + '</div></div>');
});

socket.on('getUsers', function(users){
  currentUsers = users;
  $(".connected-users .panel-body").html("");

  $(users).each(function(index){
    if(users[index] != null){
      $(".connected-users .panel-body").append('<span class="btn btn-' + colors[index % 5] + '">' + this + "</span>" );
    }
  });

});

$(document).ready(function(){
    $('#nameModal').modal('show');
    $("body").on("click", ".setNickName", function(){
    	$('#modal').modal('hide');
    	$(".form-message .nick-name span").html($("#nickName").val());
      socket.emit('saveUser', $("#nickName").val());
    });

  $('textarea').keypress(function(event) {
      if (event.keyCode == 13) {
          event.preventDefault();
         socket.emit('message', $('#msg').val(), $("#nickName").val());
         $('#msg').val('');
      }
  });

});

window.onbeforeunload = function (e) {
  socket.emit('deleteUser', $("#nickName").val());
};