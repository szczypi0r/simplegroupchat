var express = require('express'),
  app = express(),
  server = require('http').createServer(app),
  io = require('socket.io').listen(server),
  users = [];
 
 // zmienna do trzymania uzytkownikow

app.use('/static', express.static('assets'));
app.use('/bower_components',  express.static('bower_components'));

server.listen(process.env.PORT || 3000);
 
app.get('/',function(req,res){
  res.sendFile(__dirname+'/index.html');
});

 
io.sockets.on('connection', function (socket) {
 
   socket.on('message', function(msg, nickname){
     io.emit('message', msg, nickname);
   });

	socket.on('saveUser', function(nickname){
		if (users.indexOf(nickname) === -1) {
	        users.push(nickname);
	    }
		io.emit('saveUser', nickname);
		io.emit('getUsers', users);
	});

	socket.on('deleteUser', function(user){
		delete users[users.indexOf(user)];
		io.emit('getUsers', users);
   });
  
});